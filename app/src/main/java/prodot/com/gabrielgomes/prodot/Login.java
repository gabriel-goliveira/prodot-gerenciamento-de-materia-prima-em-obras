package prodot.com.gabrielgomes.prodot;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import prodot.com.gabrielgomes.prodot.email.SendEmail;
import prodot.com.gabrielgomes.prodot.user.Usuario;
import prodot.com.gabrielgomes.prodot.user.UsuarioDAO;

public class Login extends AppCompatActivity {

    private UsuarioDAO banco = new UsuarioDAO(Login.this);;
    private List<Usuario> rr = new ArrayList<>();
    private List<Usuario> rr2 = new ArrayList<>();
    private List<Usuario> rr3 = new ArrayList<>();
    private List<Usuario> rr4 = new ArrayList<>();
    ArrayList<String>tela = new ArrayList<>();
    ArrayList<String>tela2 = new ArrayList<>();
    public static final int SPLASH_TIME_OUT2 = 4000;



    @BindView(R.id.btLogin)
    Button login;

    @BindView(R.id.txtUser)
    AppCompatEditText txtUsuario;

    @BindView(R.id.txtSenha)
    AppCompatEditText txtSenha;

    @BindView(R.id.btCadastrar)
    Button cadastrar;

    @BindView(R.id.btEsqueceu)
    AppCompatTextView esqueceu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);


        verificar();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Activity.RESULT_OK == resultCode){
            Usuario usuario = new Usuario();
            usuario.setUser(data.getStringExtra("user"));
            usuario.setSenha(data.getStringExtra("senha"));


            banco.salvar(usuario);
            rr.add(usuario);
            verificar();


        }
    }

    @OnClick(R.id.btLogin)
    public void Login(){
        rr = banco.validar(txtUsuario.getText().toString(), txtSenha.getText().toString());
        if (rr.isEmpty()){
            Toast.makeText(Login.this, "Senha incorreta", Toast.LENGTH_SHORT).show();
        }else {

            Intent intent = new Intent(Login.this, MainActivity.class);
            startActivity(intent);
        }
    }

    @OnClick(R.id.btCadastrar)
    public void chamaCadastro(){
        Intent intent = new Intent(Login.this, Cadastro.class);
        startActivityForResult(intent,1 );
    }

    @OnClick(R.id.btEsqueceu)
    public void esqueceu(){

        rr3 = banco.email();
        for (Usuario u : rr3){
            tela.add(u.getUser());
        }



        rr4 = banco.senha();
        for (Usuario u : rr4){
            tela2.add(u.getSenha());
        }

        String email = tela.get(0);
        String assunto = "Recuperação de senha";
        String texto = tela2.get(0);

        SendEmail sm = new SendEmail(Login.this, email, assunto, texto);
        sm.execute();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }


        }, SPLASH_TIME_OUT2);




    }








    public void verificar(){
        rr2 = banco.verificar();
        if (rr2.isEmpty()){
            Toast.makeText(Login.this, "Bem Vindo!", Toast.LENGTH_SHORT).show();
        }else {
            cadastrar.setVisibility(View.INVISIBLE);
        }


    }






}
