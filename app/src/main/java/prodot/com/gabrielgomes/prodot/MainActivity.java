package prodot.com.gabrielgomes.prodot;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {


    private Button btProdutos;
    private Button btMona;
    private Button btRetirar;
    private AlertDialog.Builder alerta;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btProdutos = findViewById(R.id.btProdutos);
        btMona = findViewById(R.id.btMona);
        btRetirar = findViewById(R.id.retiradas);

        btProdutos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(MainActivity.this, Produtos.class);
                startActivity(it);
            }
        });

        btMona.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(MainActivity.this, Mona.class);
                startActivity(it);
            }
        });
        btRetirar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(MainActivity.this, Retirada.class);
                startActivity(it);
            }
        });


        //Collapse Toolbar codes
        final CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.collapse);
        AppBarLayout appBarLayout = findViewById(R.id.appbar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;


            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) - appBarLayout.getTotalScrollRange() == 0) {
                    // Collapsed
                    collapsingToolbarLayout.setTitle("Prodot");
                } else {
                    // Expanded
                    collapsingToolbarLayout.setTitle("");
                }
            }
        });

    }
    @Override
    public void onBackPressed() {


        alerta = new AlertDialog.Builder(MainActivity.this);
        alerta.setTitle("Deseja mesmo sair dessa página?");
        alerta.setMessage("Deseja efetuar o logout?");
        alerta.setPositiveButton("Sim",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        alerta.setNegativeButton("Não",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        alerta.create();
        alerta.show();

    }


}
