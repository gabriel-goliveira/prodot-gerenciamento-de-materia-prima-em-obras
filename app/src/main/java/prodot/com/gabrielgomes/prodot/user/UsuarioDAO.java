package prodot.com.gabrielgomes.prodot.user;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class UsuarioDAO extends SQLiteOpenHelper {

    //Construtor necessário para instanciar o objeto banco
    public UsuarioDAO(Context context){
        super(context,"Usuarios",null,1);
    }

    //Método utilizado para criar a base de dados
    @Override
    public void onCreate(SQLiteDatabase db) {

        //Criando a tabela no banco
        db.execSQL("CREATE TABLE IF NOT EXISTS Usuarios(id INTEGER PRIMARY KEY "+
                "AUTOINCREMENT, user TEXT, senha TEXT);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    //Métodos para realizar CRUD
    //1-Insert / Salvar
    //O método salvar recebe um objeto do tipo Usuario
    public Usuario salvar(Usuario usuario){
        //abri base com método para "escrever nela
        SQLiteDatabase db = getWritableDatabase();
        try {
            //Instancia de content values
            ContentValues values = new ContentValues();
            //Armazena os valores do usuario no objeto values da classe content values
            values.put("user", usuario.getUser());
            values.put("senha", usuario.getSenha());

            //Se não houver o id desse obejto no banco, ele vai armazenar a informação
            if(usuario.getId()== null){
                long id = db.insert("Usuarios",null, values);
                usuario.setId(id);
            }
        }finally {
            db.close();
        }
        return usuario;
    }

    //2- Select / buscar
    //Método do tipo lista
    //Nesse caso em específico recebe um parametro
    public List<Usuario> validar(String user, String senha){
        List<Usuario> users = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery("SELECT * "+
                    "FROM Usuarios WHERE user = '"+user+"' and senha = '"+senha+"';", null);


            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {
                Usuario usuario = new Usuario();
                usuario.setId(cursor.getLong(0));
                usuario.setUser(cursor.getString(1));
                usuario.setSenha(cursor.getString(2));
                users.add(usuario);
                cursor.moveToNext();

            }

            cursor.close();

        }catch (Exception e){
            e.printStackTrace();

        }finally {
            db.close();
        }
        return users;
    }

    public List<Usuario> verificar(){
        List<Usuario> users = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery("SELECT * "+
                    "FROM Usuarios", null);


            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {
                Usuario usuario = new Usuario();
                usuario.setId(cursor.getLong(0));
                usuario.setUser(cursor.getString(1));
                usuario.setSenha(cursor.getString(2));
                users.add(usuario);
                cursor.moveToNext();

            }

            cursor.close();

        }catch (Exception e){
            e.printStackTrace();

        }finally {
            db.close();
        }
        return users;
    }

    public List<Usuario> email(){
        List<Usuario> users = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery("SELECT * "+
                    "FROM Usuarios WHERE id = 1;", null);


            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {
                Usuario usuario = new Usuario();
                usuario.setId(cursor.getLong(0));
                usuario.setUser(cursor.getString(1));
                usuario.setSenha(cursor.getString(2));
                users.add(usuario);
                cursor.moveToNext();

            }

            cursor.close();

        }catch (Exception e){
            e.printStackTrace();

        }finally {
            db.close();
        }
        return users;
    }

    public List<Usuario> senha(){
        List<Usuario> users = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery("SELECT * "+
                    "FROM Usuarios WHERE id = 1;", null);


            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {
                Usuario usuario = new Usuario();
                usuario.setId(cursor.getLong(0));
                usuario.setUser(cursor.getString(1));
                usuario.setSenha(cursor.getString(2));
                users.add(usuario);
                cursor.moveToNext();

            }

            cursor.close();

        }catch (Exception e){
            e.printStackTrace();

        }finally {
            db.close();
        }
        return users;
    }





}


