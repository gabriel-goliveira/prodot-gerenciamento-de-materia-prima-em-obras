package prodot.com.gabrielgomes.prodot;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import prodot.com.gabrielgomes.prodot.produto.Material;
import prodot.com.gabrielgomes.prodot.produto.MaterialDAO;

public class Retirada extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    //Componentes de tela
    @BindView(R.id.textCodigoRetirada)
    TextInputLayout txtRetirada;

    @BindView(R.id.buscar)
    AppCompatButton btBuscar;

    @BindView(R.id.nameP)
    TextView nome;

    @BindView(R.id.marcaP)
    TextView marca;

    @BindView(R.id.descP)
    TextView desc;

    @BindView(R.id.medidaP)
    TextView medida;

    @BindView(R.id.btRetirar)
    AppCompatButton btRetirar;


    //Navigation Drawer

    @BindView(R.id.toolbar3)
    Toolbar toolbar;

    @BindView(R.id.drawerLayoutR)
    DrawerLayout drawer;

    @BindView(R.id.navViewR)
    NavigationView navigation;


    //Banco
    List<Material> materials = new ArrayList<>();
    private MaterialDAO banco  = new MaterialDAO(this);
    ArrayList<Material>tela = new ArrayList<>();

    //Recebe id
    private Long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retirada);
        ButterKnife.bind(this);



        //Instanciando navdrawer
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar , R.string.open_drawer2 , R.string.close_drawer2);
        drawer.addDrawerListener(toggle);

        toggle.syncState();

        navigation.setNavigationItemSelectedListener(this);

    }

    @OnClick(R.id.buscar)
    public void buscar(){

        materials = banco.procurarCodigo(txtRetirada.getEditText().getText().toString());

        for (Material r : materials){
            tela.add(r);
        }

        if(!tela.isEmpty()) {

            id = tela.get(0).getId();
            nome.setText(tela.get(0).getNome());
            marca.setText(tela.get(0).getMarca());
            desc.setText(tela.get(0).getDesc());
            medida.setText(tela.get(0).getMedida());
            btRetirar.setEnabled(true);

        }else {
            Toast.makeText(Retirada.this, "Não há Registros", Toast.LENGTH_SHORT).show();
        }

    }


    @OnClick(R.id.btRetirar)
    public void retirar(){
        banco.Retirar(id);
        nome.setText(" ");
        marca.setText(" ");
        desc.setText(" ");
        medida.setText(" ");
        btRetirar.setEnabled(false);
    }


    //Método para recuperar click no botao

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.produtos_m: {
                Intent it =new Intent(Retirada.this, Produtos.class);
                startActivity(it);
                break;
            }
            case R.id.retirada_m: {
                Toast.makeText(this, "Você está na página de retirada", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.mona_m: {
                Intent it =new Intent(Retirada.this, Mona.class);
                startActivity(it);
                break;
            }


        }



        return true;
    }


    //Fechar navdrawer quando clicar no botao voltar
    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
