package prodot.com.gabrielgomes.prodot;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CadastroProdutos extends AppCompatActivity {


    @BindView(R.id.textCodigo)
    TextInputLayout txtCodigo;

    @BindView(R.id.textNome)
    TextInputLayout txtNome;

    @BindView(R.id.textMarca)
    TextInputLayout txtMarca;

    @BindView(R.id.textDesc)
    TextInputLayout txtDesc;

    @BindView(R.id.textMedida)
    TextInputLayout txtMedida;

    @BindView(R.id.nfc)
    AppCompatButton nfc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_produtos);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btCadastrarP)
    public void CadastrarProduto(){
        Intent it = new Intent();
        it.putExtra("codigo", txtCodigo.getEditText().getText().toString());
        it.putExtra("nome", txtNome.getEditText().getText().toString());
        it.putExtra("marca", txtMarca.getEditText().getText().toString());
        it.putExtra("desc", txtDesc.getEditText().getText().toString());
        it.putExtra("medida", txtMedida.getEditText().getText().toString());


        setResult(Activity.RESULT_OK, it);
        finish();
    }

    @OnClick(R.id.nfc)
    public void gerar(){
        txtCodigo.getEditText().setText("123456");
    }
}
