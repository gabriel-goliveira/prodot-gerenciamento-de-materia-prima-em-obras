package prodot.com.gabrielgomes.prodot.produto;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.dift.ui.SwipeToAction;
import prodot.com.gabrielgomes.prodot.R;

public class MaterialAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Material> items;
    public class MaterialViewHolder extends SwipeToAction.ViewHolder<Material> {
        public TextView titleView;
        public TextView descView;


        public MaterialViewHolder(View v) {
            super(v);

            titleView = (TextView) v.findViewById(R.id.title);
            descView = (TextView) v.findViewById(R.id.desc);

        }
    }

    /** Constructor **/
    public MaterialAdapter(List<Material> items) {
        this.items = items;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_view, parent, false);

        return new MaterialViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Material item = items.get(position);
        MaterialViewHolder vh = (MaterialViewHolder) holder;
        vh.titleView.setText(item.getCodigo() + " | " + item.getNome() + " | " + item.getMarca());
        vh.descView.setText(item.getDesc() + " | " + item.getMedida());

        vh.data = item;
    }
}
