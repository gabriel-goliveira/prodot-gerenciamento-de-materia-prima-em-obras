package prodot.com.gabrielgomes.prodot;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Cadastro extends AppCompatActivity {
    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.password)
    EditText senha;

    @BindView(R.id.textEmail)
    TextInputLayout txtEmail;

    @BindView(R.id.textSenha)
    TextInputLayout txtSenha;

   private AppCompatButton cadastrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        ButterKnife.bind(this);

        cadastrar = findViewById(R.id.cadastrar);
        cadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent();
                it.putExtra("user", txtEmail.getEditText().getText().toString());
                it.putExtra("senha", txtSenha.getEditText().getText().toString());


                setResult(Activity.RESULT_OK, it);
                finish();
            }
        });
    }


}
