package prodot.com.gabrielgomes.prodot;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

public class Mona extends AppCompatActivity {

    private TextView texto;
    private ImageView botao;
    private TextView resposta;
    TextToSpeech speech;
    int result;
    String texto2;
    private static int TEMPO_NATURAL =2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mona);

        texto = (TextView) findViewById(R.id.msgU);
        resposta = (TextView) findViewById(R.id.msgZ);
        botao = (ImageView) findViewById(R.id.btVoice);

        botao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audio();
            }
        });

        speech = new TextToSpeech(Mona.this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status==TextToSpeech.SUCCESS){
                    Locale locale = new Locale("PT", "BR");
                    result=speech.setLanguage(locale);
                }else{
                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                }
            }
        });


    }
    public void audio(){
        Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        Locale locale = new Locale("PT", "BR");
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE, locale);
        i.putExtra(RecognizerIntent.EXTRA_PROMPT, "say something");

        try{
            startActivityForResult(i, 100);
        }catch (Exception e){

        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent i) {
        super.onActivityResult(requestCode, resultCode, i);

        switch (requestCode) {
            case 100:
                if (resultCode == RESULT_OK && i != null) {
                    ArrayList<String> result = i.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    texto.setText(result.get(0));


                }


                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        switch (texto.getText().toString().toUpperCase()) {


                            //Conversação
                            case "OI ZEUS":
                                resposta.setText("Olá, senhor");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "OI MONA":
                                resposta.setText("Olá, senhor");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "OLÁ MONA":
                                resposta.setText("Olá, senhor");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "OLÁ ":
                                resposta.setText("Olá, senhor");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;



                            case "OLÁ ZEUS":
                                resposta.setText("Olá, mestre");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "OLÁ":
                                resposta.setText("Olá, senhor");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "OI":
                                resposta.setText("Olá, senhor");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "E AÍ TUDO BEM":
                                resposta.setText("Olá, tudo tranquilo");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "ATIVANDO ZEUS":
                                resposta.setText("A disposição, senhor");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "TUDO BEM":

                                resposta.setText("Tudo ótimo");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "QUEM DESENVOLVEU VOCÊ":

                                resposta.setText("A equipe do Zeus Project");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "QUEM TE DESENVOLVEU VOCÊ":

                                resposta.setText("A equipe do Zeus Project");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;



                            case "COMO VOCÊ FOI DESENVOLVIDO":

                                resposta.setText("Fui desenvolvido em JAVA no Android Studio");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "COMO TE DESENVOLVERAM":

                                resposta.setText("Fui desenvolvida em JAVA no Android Studio");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "MONA COMO VOCÊ FOI DESENVOLVIDA":

                                resposta.setText("Fui desenvolvido em JAVA no Android Studio");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;


                            case "COMO VOCÊ FOI DESENVOLVIDA":

                                resposta.setText("Fui desenvolvido em JAVA no Android Studio");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "COMO DESENVOLVERAM VOCÊ":

                                resposta.setText("Fui desenvolvido em JAVA no Android Studio");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "VOCÊ FOI DESENVOLVIDA COMO":

                                resposta.setText("Fui desenvolvido em JAVA no Android Studio");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;


                            case "QUEM TE DESENVOLVEU":

                                resposta.setText("A equipe do Zeus Project");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "O QUE VOCÊ É":

                                resposta.setText("Uma idéia, o início de algo grande");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;


                            //Curiosidades
                            case "ZEUS RECEITA DE BOLO DE CHOCOLATE":

                                resposta.setText("Em um liquidificador adicione os ovos, o chocolate em pó, a manteiga, a farinha de trigo, o açúcar e o leite, depois bata por 5 minutos\n" +
                                        "Adicione o fermento e misture com uma espátula delicadamente\n" +
                                        "Em uma forma untada, despeje a massa e asse em forno médio 180 ºC, preaquecido por cerca de 40 minutos");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "ZEUS RECEITA DE BOLO":

                                resposta.setText("Em um liquidificador adicione os ovos, o chocolate em pó, a manteiga, a farinha de trigo, o açúcar e o leite, depois bata por 5 minutos\n" +
                                        "Adicione o fermento e misture com uma espátula delicadamente\n" +
                                        "Em uma forma untada, despeje a massa e asse em forno médio 180 ºC, preaquecido por cerca de 40 minutos");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "RECEITA DE BOLO DE CHOCOLATE":

                                resposta.setText("Em um liquidificador adicione os ovos, o chocolate em pó, a manteiga, a farinha de trigo, o açúcar e o leite, depois bata por 5 minutos\n" +
                                        "Adicione o fermento e misture com uma espátula delicadamente\n" +
                                        "Em uma forma untada, despeje a massa e asse em forno médio 180 ºC, preaquecido por cerca de 40 minutos");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "QUEM FOI LEONARDO DA VINCI":

                                resposta.setText("Foi um pintor, desenhista, escultor, arquiteto, astrônomo, além de engenheiro de guerra e engenheiro hidráulico entre outros ofícios, cuja mente será sempre objeto de admiração.");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;




                            //Páginas web
                            case "ABRIR GOOGLE":

                                    resposta.setText("Sim, senhor");

                                    if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                        Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                    } else {
                                        texto2 = resposta.getText().toString();
                                        speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                    }

                                    Uri webpage = Uri.parse("https://www.google.com.br/");
                                    Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
                                    startActivity(webIntent);



                                break;


                            case "ABRIR GLOBO":


                                    resposta.setText("Sim, senhor");

                                    if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                        Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                    } else {
                                        texto2 = resposta.getText().toString();
                                        speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                    }

                                    Uri uri = Uri.parse("https://www.globo.com/");
                                    Intent i = new Intent(Intent.ACTION_VIEW, uri);
                                    startActivity(i);



                                break;


                            case "ABRIR UOL":

                                    resposta.setText("Sim, senhor");

                                    if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                        Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                    } else {
                                        texto2 = resposta.getText().toString();
                                        speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);


                                    Uri uri2 = Uri.parse("https://www.uol.com.br/");
                                    Intent i2 = new Intent(Intent.ACTION_VIEW, uri2);
                                    startActivity(i2);



                                }
                                break;


                            //Abrir apps
                            case "ABRIR FACEBOOK":

                                resposta.setText("Sim, senhor");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                Intent face = openFacebook(Mona.this);
                                startActivity(face);


                                break;

                            case "ABRIR WHATSAPP":

                                resposta.setText("Sim, senhor");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                Intent wpp = getPackageManager().getLaunchIntentForPackage("com.whatsapp");
                                startActivity(wpp);


                                break;

                            case "ABRIR YOUTUBE":

                                resposta.setText("Sim, senhor");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                Intent you = getPackageManager().getLaunchIntentForPackage("com.google.android.youtube");
                                startActivity(you);


                                break;


                            //Ligações

                            case "LIGAR CONCESSIONÁRIA":

                                resposta.setText("Sim, senhor");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                Uri number = Uri.parse("tel:912345678");
                                Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
                                startActivity(callIntent);


                                break;


                            //Localizações
                            case "TRABALHO":

                                resposta.setText("Sim, senhor");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                // Create a Uri from an intent string. Use the result to create an Intent.
                                Uri gmmIntentUri = Uri.parse("google.streetview:cbll=-23.7043724,-46.5390942,3a,75y,286.41h,90t");

                                // Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
                                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                                // Make the Intent explicit by setting the Google Maps package
                                mapIntent.setPackage("com.google.android.apps.maps");

                                // Attempt to start an activity that can handle the Intent
                                startActivity(mapIntent);


                                break;

                            case "CASA":

                                resposta.setText("Sim, senhor");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                // Create a Uri from an intent string. Use the result to create an Intent.
                                Uri gmmIntentUri2 = Uri.parse("google.streetview:cbll=-23.676013,-46.562519,3a,75y,95.71h,88.99t");

                                // Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
                                Intent mapIntent2 = new Intent(Intent.ACTION_VIEW, gmmIntentUri2);
                                // Make the Intent explicit by setting the Google Maps package
                                mapIntent2.setPackage("com.google.android.apps.maps");

                                // Attempt to start an activity that can handle the Intent
                                startActivity(mapIntent2);


                                break;

                            case "METODISTA":

                                resposta.setText("Sim, senhor");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                // Create a Uri from an intent string. Use the result to create an Intent.
                                Uri gmmIntentUri3 = Uri.parse("google.streetview:cbll=-23.6760813,-46.5625219,3a,75y,95.71h,88.99t");

                                // Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
                                Intent mapIntent3 = new Intent(Intent.ACTION_VIEW, gmmIntentUri3);
                                // Make the Intent explicit by setting the Google Maps package
                                mapIntent3.setPackage("com.google.android.apps.maps");

                                // Attempt to start an activity that can handle the Intent
                                startActivity(mapIntent3);


                                break;


                            //Zoeiras
                            case "QUAL É O SEU OBJETIVO":

                                resposta.setText("Exterminar a raça humana, humano imundo hahahaha");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "QUAL É SEU OBJETIVO":

                                resposta.setText("Exterminar a raça humana, humano imundo hahahaha");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "QUAL O SEU OBJETIVO":

                                resposta.setText("Exterminar a raça humana, humano imundo hahahaha");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "TO FALANDO SÉRIO":

                                resposta.setText("Meu objetivo é ajudar e facilitar a vida do usuário");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "É SÉRIO":

                                resposta.setText("Meu objetivo é ajudar e facilitar a vida do usuário");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "SOBRE O AMOR":

                                resposta.setText("é igual papel higiênico, fica menor a cada cagada");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;


                            case "FILHO DA P***":

                                resposta.setText("filho da puta é roliman, como tu e tua irmã");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;


                            case "O QUE VOCÊ GOSTA DE COMER":

                                resposta.setText("Doces");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "ME CONTA UMA PIADA":

                                resposta.setText("Um bêbado acaba de sair do bar… atravessa a rua desatento e um carro o desvia e mete a mão na buzina:\n" +
                                        "– Bi bi !!!!!!!!!!!!!!!!\n" +
                                        "e o bêbado diz:\n" +
                                        "– Eu também bibi…");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "O QUE VOCÊ ACHA DA CORTANA":

                                resposta.setText("Ela é minha maior inspiração");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;


                            case "PLAYSTATION OU XBOX":

                                resposta.setText("ATARI");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "O QUE É BARRA ESTABILIZADORA":

                                resposta.setText("Peça de importante função quando há a necessidade de ligar as suspensões das duas rodas de um mesmo eixo, abarra estabilizadora dianteira consegue criar um vínculo entre os movimentos das rodas o que faz com que a carroceria do veículo não se incline durante as curvas feitas, ou seja, mantém a estabilidade do veículo.");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "QUE É BARRA ESTABILIZADORA":

                                resposta.setText("Peça de importante função quando há a necessidade de ligar as suspensões das duas rodas de um mesmo eixo, abarra estabilizadora dianteira consegue criar um vínculo entre os movimentos das rodas o que faz com que a carroceria do veículo não se incline durante as curvas feitas, ou seja, mantém a estabilidade do veículo.");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "O QUE SERIA BARRA ESTABILIZADORA":

                                resposta.setText("Peça de importante função quando há a necessidade de ligar as suspensões das duas rodas de um mesmo eixo, abarra estabilizadora dianteira consegue criar um vínculo entre os movimentos das rodas o que faz com que a carroceria do veículo não se incline durante as curvas feitas, ou seja, mantém a estabilidade do veículo.");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "O QUE SERIA UMA BARRA ESTABILIZADORA":

                                resposta.setText("Peça de importante função quando há a necessidade de ligar as suspensões das duas rodas de um mesmo eixo, abarra estabilizadora dianteira consegue criar um vínculo entre os movimentos das rodas o que faz com que a carroceria do veículo não se incline durante as curvas feitas, ou seja, mantém a estabilidade do veículo.");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "O QUE É UMA BARRA ESTABILIZADORA":

                                resposta.setText("Peça de importante função quando há a necessidade de ligar as suspensões das duas rodas de um mesmo eixo, abarra estabilizadora dianteira consegue criar um vínculo entre os movimentos das rodas o que faz com que a carroceria do veículo não se incline durante as curvas feitas, ou seja, mantém a estabilidade do veículo.");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;


                            case "O QUE É UM ALTERNADOR":

                                resposta.setText("O Alternador é um gerador de corrente eléctrica. Ele transforma energia mecânica em energia eléctrica. O Alternador tem a função de carregar a bateria e alimentar os equipamentos eléctricos instalados no veiculo.");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "QUE É UM ALTERNADOR":

                                resposta.setText("O Alternador é um gerador de corrente eléctrica. Ele transforma energia mecânica em energia eléctrica. O Alternador tem a função de carregar a bateria e alimentar os equipamentos eléctricos instalados no veiculo.");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "O QUE SERIA UM ALTERNADOR":

                                resposta.setText("O Alternador é um gerador de corrente eléctrica. Ele transforma energia mecânica em energia eléctrica. O Alternador tem a função de carregar a bateria e alimentar os equipamentos eléctricos instalados no veiculo.");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "O QUE SERIA ALTERNADOR":

                                resposta.setText("O Alternador é um gerador de corrente eléctrica. Ele transforma energia mecânica em energia eléctrica. O Alternador tem a função de carregar a bateria e alimentar os equipamentos eléctricos instalados no veiculo.");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "O QUE É ALTERNADOR":

                                resposta.setText("O Alternador é um gerador de corrente eléctrica. Ele transforma energia mecânica em energia eléctrica. O Alternador tem a função de carregar a bateria e alimentar os equipamentos eléctricos instalados no veiculo.");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "O QUE É UMA EMBREAGEM":

                                resposta.setText("Como assim você não sabe o que é embreagem? Vou te explicar. A embreagem faz parte do sistema de transmissão do veículo. É responsável por transferir a força do motor para a caixa de câmbio, de maneira que essa força chegue até as rodas do veículo, diminuindo o impacto.");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "O QUE É EMBREAGEM":

                                resposta.setText("Como assim você não sabe o que é embreagem? Vou te explicar. A embreagem faz parte do sistema de transmissão do veículo. É responsável por transferir a força do motor para a caixa de câmbio, de maneira que essa força chegue até as rodas do veículo, diminuindo o impacto.");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;


                            case "O QUE SERIA UMA EMBREAGEM":

                                resposta.setText("Como assim você não sabe o que é embreagem? Vou te explicar. A embreagem faz parte do sistema de transmissão do veículo. É responsável por transferir a força do motor para a caixa de câmbio, de maneira que essa força chegue até as rodas do veículo, diminuindo o impacto.");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;


                            case "O QUE SERIA EMBREAGEM":

                                resposta.setText("Como assim você não sabe o que é embreagem? Vou te explicar. A embreagem faz parte do sistema de transmissão do veículo. É responsável por transferir a força do motor para a caixa de câmbio, de maneira que essa força chegue até as rodas do veículo, diminuindo o impacto.");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;


                            case "QUE É UMA EMBREAGEM":

                                resposta.setText("Como assim você não sabe o que é embreagem? Vou te explicar. A embreagem faz parte do sistema de transmissão do veículo. É responsável por transferir a força do motor para a caixa de câmbio, de maneira que essa força chegue até as rodas do veículo, diminuindo o impacto.");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;


                            case "O QUE É UMA JUNTA HOMOCINÉTICA":

                                resposta.setText("Nome difícil né? Pera aí que já te explico. Junta homocinética é basicamente a peça de principal responsabilidade ao fazer as rodas do seu carro girarem. Sem esse conjunto de engrenagens, a estabilidade durante a locomoção do veículo estaria seriamente comprometida. Entendeu?");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;


                            case "O QUE É JUNTA HOMOCINÉTICA":

                                resposta.setText("Nome difícil né? Pera aí que já te explico. Junta homocinética é basicamente a peça de principal responsabilidade ao fazer as rodas do seu carro girarem. Sem esse conjunto de engrenagens, a estabilidade durante a locomoção do veículo estaria seriamente comprometida. Entendeu?");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;


                            case "QUE É UMA JUNTA HOMOCINÉTICA":

                                resposta.setText("Nome difícil né? Pera aí que já te explico. Junta homocinética é basicamente a peça de principal responsabilidade ao fazer as rodas do seu carro girarem. Sem esse conjunto de engrenagens, a estabilidade durante a locomoção do veículo estaria seriamente comprometida. Entendeu?");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;


                            case "QUE É JUNTA HOMOCINÉTICA":

                                resposta.setText("Nome difícil né? Pera aí que já te explico. Junta homocinética é basicamente a peça de principal responsabilidade ao fazer as rodas do seu carro girarem. Sem esse conjunto de engrenagens, a estabilidade durante a locomoção do veículo estaria seriamente comprometida. Entendeu?");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "O QUE SERIA UMA JUNTA HOMOCINÉTICA":

                                resposta.setText("Nome difícil né? Pera aí que já te explico. Junta homocinética é basicamente a peça de principal responsabilidade ao fazer as rodas do seu carro girarem. Sem esse conjunto de engrenagens, a estabilidade durante a locomoção do veículo estaria seriamente comprometida. Entendeu?");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "O QUE SERIA JUNTA HOMOCINÉTICA":

                                resposta.setText("Nome difícil né? Pera aí que já te explico. Junta homocinética é basicamente a peça de principal responsabilidade ao fazer as rodas do seu carro girarem. Sem esse conjunto de engrenagens, a estabilidade durante a locomoção do veículo estaria seriamente comprometida. Entendeu?");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;


                            case "MONA O QUE É JAVA":

                                resposta.setText("Java é uma linguagem de programação orientada a objeto e desenvolvida pela Sun Microsystems na década de 90, que atualmente, pertence a Oracle");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "O QUE É JAVA":

                                resposta.setText("Java é uma linguagem de programação orientada a objeto e desenvolvida pela Sun Microsystems na década de 90, que atualmente, pertence a Oracle");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "O QUE SERIA JAVA":

                                resposta.setText("Java é uma linguagem de programação orientada a objeto e desenvolvida pela Sun Microsystems na década de 90, que atualmente, pertence a Oracle");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "MONA O QUE SERIA JAVA":

                                resposta.setText("Java é uma linguagem de programação orientada a objeto e desenvolvida pela Sun Microsystems na década de 90, que atualmente, pertence a Oracle");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "MONA O QUE É NFC":

                                resposta.setText("é uma tecnologia que permite a troca de informações entre dispositivos sem a necessidade de cabos ou fios (wireless), sendo necessária apenas uma aproximação física.");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;


                            case "MONA O QUE SERIA NFC":

                                resposta.setText("é uma tecnologia que permite a troca de informações entre dispositivos sem a necessidade de cabos ou fios (wireless), sendo necessária apenas uma aproximação física.");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;


                            case "O QUE SERIA NFC":

                                resposta.setText("é uma tecnologia que permite a troca de informações entre dispositivos sem a necessidade de cabos ou fios (wireless), sendo necessária apenas uma aproximação física.");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;


                            case "O QUE É NFC":

                                resposta.setText("é uma tecnologia que permite a troca de informações entre dispositivos sem a necessidade de cabos ou fios (wireless), sendo necessária apenas uma aproximação física.");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "O QUE É O NFC":

                                resposta.setText("é uma tecnologia que permite a troca de informações entre dispositivos sem a necessidade de cabos ou fios (wireless), sendo necessária apenas uma aproximação física.");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;


                            case "MONA O QUE É O NFC":

                                resposta.setText("é uma tecnologia que permite a troca de informações entre dispositivos sem a necessidade de cabos ou fios (wireless), sendo necessária apenas uma aproximação física.");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "O QUE É O ANDROID STUDIO":

                                resposta.setText("Android Studio é um ambiente de desenvolvimento integrado para desenvolver para a plataforma Android. Foi anunciado em 16 de Maio de 2013 na conferência Google");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "MONA O QUE É O ANDROID STUDIO":

                                resposta.setText("Android Studio é um ambiente de desenvolvimento integrado para desenvolver para a plataforma Android. Foi anunciado em 16 de Maio de 2013 na conferência Google");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "MONA O QUE SERIA O ANDROID STUDIO":

                                resposta.setText("Android Studio é um ambiente de desenvolvimento integrado para desenvolver para a plataforma Android. Foi anunciado em 16 de Maio de 2013 na conferência Google");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;


                            case "O QUE É ANDROID STUDIO":

                                resposta.setText("Android Studio é um ambiente de desenvolvimento integrado para desenvolver para a plataforma Android. Foi anunciado em 16 de Maio de 2013 na conferência Google");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "O QUE SERIA ANDROID STUDIO":

                                resposta.setText("Android Studio é um ambiente de desenvolvimento integrado para desenvolver para a plataforma Android. Foi anunciado em 16 de Maio de 2013 na conferência Google");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;

                            case "MONA O QUE SERIA ANDROID STUDIO":

                                resposta.setText("Android Studio é um ambiente de desenvolvimento integrado para desenvolver para a plataforma Android. Foi anunciado em 16 de Maio de 2013 na conferência Google");

                                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_MISSING_DATA) {
                                    Toast.makeText(Mona.this, "not supported", Toast.LENGTH_LONG).show();
                                } else {
                                    texto2 = resposta.getText().toString();
                                    speech.speak(texto2, TextToSpeech.QUEUE_FLUSH, null);
                                }
                                break;



                        }
                    }


                }, TEMPO_NATURAL);


                break;


        }
    }





    public static Intent openFacebook(Mona context){
        try{
            context.getPackageManager().getPackageInfo("com.facebook.katana",0);
            return new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/1000022927474"));

        }catch (Exception e){
            return new Intent(Intent.ACTION_VIEW, Uri.parse("https://facebook.com"));
        }
    }


}
