package prodot.com.gabrielgomes.prodot;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.dift.ui.SwipeToAction;
import prodot.com.gabrielgomes.prodot.produto.Material;
import prodot.com.gabrielgomes.prodot.produto.MaterialAdapter;
import prodot.com.gabrielgomes.prodot.produto.MaterialDAO;

public class Produtos extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawerLayout)
    DrawerLayout drawer;

    @BindView(R.id.navView)
    NavigationView navigation;

    @BindView(R.id.recycler)
    RecyclerView recyclerView;



    MaterialAdapter adapter;

    SwipeToAction swipeToAction;


    List<Material> materials = new ArrayList<>();
    private MaterialDAO banco;
    ArrayList<Material>tela = new ArrayList<>();






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produtos);
        ButterKnife.bind(this);

        banco = new MaterialDAO(this);
        materials = banco.listar();

        for (Material r : materials){
            tela.add(r);
        }


        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        populate();
        adapter = new MaterialAdapter(this.tela);
        recyclerView.setAdapter(adapter);

        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar , R.string.open_drawer , R.string.close_drawer);
        drawer.addDrawerListener(toggle);

        toggle.syncState();

        navigation = (NavigationView) findViewById(R.id.navView);
        navigation.setNavigationItemSelectedListener(this);

        swipeToAction = new SwipeToAction(recyclerView, new SwipeToAction.SwipeListener<Material>() {
            @Override
            public boolean swipeLeft(final Material itemData) {
                final int pos = removeMaterial(itemData);
                adapter.notifyDataSetChanged();
                populate();

                displaySnackbar(itemData.getNome() + " removido", "Desfazer", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Material restore = new Material();
                        restore.setCodigo(itemData.getCodigo());
                        restore.setNome(itemData.getNome());
                        restore.setMarca(itemData.getMarca());
                        restore.setDesc(itemData.getDesc());
                        restore.setMedida(itemData.getMedida());
                        banco.salvar(restore);
                        tela.add(restore);
                        adapter.notifyDataSetChanged();
                    }
                });
                return true;
            }

            @Override
            public boolean swipeRight(Material itemData) {
                displaySnackbar(itemData.getNome() + " loved", null, null);
                return true;
            }

            @Override
            public void onClick(Material itemData) {
                displaySnackbar(itemData.getNome() + " clicked", null, null);
            }

            @Override
            public void onLongClick(Material itemData) {
                displaySnackbar(itemData.getNome() + " long clicked", null, null);
            }
        });


    }
    private void displaySnackbar(String text, String actionName, View.OnClickListener action) {
        Snackbar snack = Snackbar.make(findViewById(android.R.id.content), text, Snackbar.LENGTH_LONG)
                .setAction(actionName, action);

        View v = snack.getView();
        v.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        ((TextView) v.findViewById(android.support.design.R.id.snackbar_text)).setTextColor(Color.WHITE);
        ((TextView) v.findViewById(android.support.design.R.id.snackbar_action)).setTextColor(Color.WHITE);

        snack.show();
    }

    private int removeMaterial(Material material) {
        Long pos = material.getId();
        banco.Retirar(pos);
        int posi = materials.indexOf(material);
        materials.remove(material);
        tela.remove(material);
        adapter.notifyItemRemoved(posi);
        return posi;
    }





    @OnClick(R.id.fb)
    public void chamarCadastro(){
        Intent it = new Intent(Produtos.this, CadastroProdutos.class);
        startActivityForResult(it,1 );
    }




    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {
                case R.id.produtos_m: {
                    Toast.makeText(this, "Você está na pagina produtos", Toast.LENGTH_SHORT).show();
                    break;
                }
                case R.id.retirada_m: {
                    Intent it = new Intent(Produtos.this, Retirada.class);
                    startActivity(it);
                    break;
                }
                case R.id.mona_m: {
                    Intent it =new Intent(Produtos.this, Mona.class);
                    startActivity(it);
                    break;
                }


        }



        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Activity.RESULT_OK == resultCode){
            Material material = new Material();
            material.setCodigo(data.getStringExtra("codigo"));
            material.setNome(data.getStringExtra("nome"));
            material.setMarca(data.getStringExtra("marca"));
            material.setDesc(data.getStringExtra("desc"));
            material.setMedida(data.getStringExtra("medida"));

            if(materials.isEmpty()){
                tela.remove(0);
                banco.salvar(material);
                materials.add(material);
                tela.add(material);
                adapter.notifyDataSetChanged();
            }else {


                banco.salvar(material);
                materials.add(material);
                tela.add(material);
                adapter.notifyDataSetChanged();

            }


        }
    }
    private void populate(){

        if(materials.isEmpty()){
            Material m = new Material();
            m.setId(1L);
            m.setCodigo("123456789");
            m.setNome("Produto de Exemplo");
            m.setMarca("Marca de teste");
            m.setDesc("Esse produto de Exemplo é excluído após a entrada do primeiro produto");
            m.setMedida("KG");
            this.tela.add(m);



        }



    }

}
