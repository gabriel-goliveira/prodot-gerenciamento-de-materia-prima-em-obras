package prodot.com.gabrielgomes.prodot.produto;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class MaterialDAO  extends SQLiteOpenHelper {

    public MaterialDAO(Context context){
        super(context,"Material",null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS Material(id INTEGER PRIMARY KEY "+
                "AUTOINCREMENT, codigo TEXT, nome TEXT, marca TEXT, descricao TEXT, medida TEXT);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public Material salvar(Material material){
        SQLiteDatabase db = getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put("codigo", material.getCodigo());
            values.put("nome", material.getNome());
            values.put("marca", material.getMarca());
            values.put("descricao", material.getDesc());
            values.put("medida", material.getMedida());



            if(material.getId()== null){
                long id = db.insert("Material",null, values);
                material.setId(id);
            }else {
                values.put("codigo", material.getCodigo());
                values.put("nome", material.getNome());
                values.put("marca", material.getMarca());
                values.put("descricao", material.getDesc());
                values.put("medida", material.getMedida());
                String[] where = new String[]{String.valueOf(material.getId())};
            }
        }finally {
            db.close();
        }
        return material;
    }

    public List<Material> listar(){
        List<Material> todos = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery("SELECT * "+
                    "FROM Material", null);

            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {
                Material material =  new Material();
                material.setId(cursor.getLong(0));
                material.setCodigo(cursor.getString(1));
                material.setNome(cursor.getString(2));
                material.setMarca(cursor.getString(3));
                material.setDesc(cursor.getString(4));
                material.setMedida(cursor.getString(5));
                todos.add(material);
                cursor.moveToNext();

            }

            cursor.close();

        }catch (Exception e){
            e.printStackTrace();

        }finally {
            db.close();
        }
        return todos;
    }

    public List<Material> procurarCodigo(String codigo){
        List<Material> materials = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery("SELECT * "+
                    "FROM Material WHERE codigo = '"+codigo+"';", null);


            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {
                Material material = new Material();
                material.setId(cursor.getLong(0));
                material.setCodigo(cursor.getString(1));
                material.setNome(cursor.getString(2));
                material.setMarca(cursor.getString(3));
                material.setDesc(cursor.getString(4));
                material.setMedida(cursor.getString(5));
                materials.add(material);
                cursor.moveToNext();

            }

            cursor.close();

        }catch (Exception e){
            e.printStackTrace();

        }finally {
            db.close();
        }
        return materials;
    }


    public void Retirar(Long id){
        SQLiteDatabase db = getWritableDatabase();
        String where [] = new String[] { String.valueOf(id) };
        db.delete("Material", "id = ?", where);
        db.close();
    }

}
